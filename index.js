// mock database
let posts = [];

// post id
let count = 1;




// Add post data----------------------------------------------------------------------
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
	//prevents the page from loading, 
	// prevents the default behavior of the event
	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});

	// increment the id value in count variable
	count++;


	alert("Post successfully added!");
	showPosts(posts);
});



// RETRIEVE POST DATA ----------------------------------------------------------------
const showPosts = () => {
	let postEntries = "";

	posts.forEach((post) => {
		postEntries += `
			<div id ="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});
	console.log(postEntries);
	document.querySelector("#div-post-entries").innerHTML = postEntries;

};


// EDIT POST ---------------------------------------------------------------------------

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
};

// UPDATE POST 
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
	e.preventDefault();

	for (let i = 0; i < posts.length; i++) {

		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value){
			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value;

			showPosts(posts);
			alert("Successfully updated!");

			break;
		};
	};
});


// DELETE POST------------------------------------------------------------------------------

const deletePost = (id) => {

	for(let i=0; i<posts.length; i++){

		if(posts[i].id.toString() === id)
		{
			posts.splice(i, 1)
			break;
		}
	}

	showPosts(posts);
	
	alert('Post successfully deleted!')
};
